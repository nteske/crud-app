package rva.ctrls;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import rva.jpa.Departman;
import rva.jpa.Fakultet;
import rva.reps.DepartmanRepository;
import rva.reps.FakultetRepository;

@Api(tags = {"Departman CRUD operacije"})
@RestController
public class DepartmanRestController {
	@Autowired
	private FakultetRepository fakultetRepository;
	@Autowired
	private DepartmanRepository departmanRepository;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@ApiOperation(value = "Vraća kolekciju svih departmana iz baze podataka koji pripadaju fakultetu")
	@GetMapping("departmaniZaFakultetById/{id}")
	public Collection<Departman> departmaniZaFakultetById(@PathVariable("id") Integer id)
	{
		Fakultet f=fakultetRepository.getOne(id);
		return departmanRepository.findByFakultet(f);
	}
	@ApiOperation(value = "Vraća kolekciju svih departmana iz baze podataka")
	@GetMapping("departman")
	public Collection<Departman> getDepartman() {
		return departmanRepository.findAll();
	}
	@ApiOperation(value = "Vraća departman iz baze podataka čiji je id vrednost prosleđena kao path varijabla")
	@GetMapping("departman/{id}")
	public Departman getDepartman(@PathVariable("id") Integer id) {
		return departmanRepository.getOne(id);
	}
	
	@ApiOperation(value = "Vraća kolekciju svih departmana iz baze podataka koji u nazivu sadrže string prosleđen kao path varijabla")
	@GetMapping("departmanNaziv/{naziv}")
	public Collection<Departman> getDepartmanByNaziv(@PathVariable("naziv") String naziv) {
		return departmanRepository.findByNazivContainingIgnoreCase(naziv);
	}
	
	@ApiOperation(value = "Briše departman iz baze podataka čiji je id vrednost prosleđena kao path varijabla")
	@Transactional
	@CrossOrigin
	@DeleteMapping("departman/{id}")
	public ResponseEntity<Departman> deleteDepartman(@PathVariable("id") Integer id) {
		if(!departmanRepository.existsById(id))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		departmanRepository.deleteById(id);
		departmanRepository.flush();
		if(id==6)
			jdbcTemplate.execute("INSERT INTO \"departman\" (\"id\",\"naziv\",\"oznaka\",\"fakultet\") VALUES (6,'TEST Departman','T O',1)");
		
		jdbcTemplate.execute("delete from student where departman = "+id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	// insert
	@ApiOperation(value = "Upisuje departman u bazu podataka")
	@CrossOrigin
	@PostMapping("departman")
	public ResponseEntity<Departman> insertDepartman(@RequestBody Departman departman) {
		if(!departmanRepository.existsById(departman.getId())) {
			departmanRepository.save(departman);
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
	
	// update
	@ApiOperation(value = "Modifikuje postojeći departman u bazi podataka")
	@CrossOrigin
	@PutMapping("departman")
	public ResponseEntity<Departman> updateDepartman(@RequestBody Departman departman) {
		if (!departmanRepository.existsById(departman.getId()))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		departmanRepository.save(departman);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	 
}
