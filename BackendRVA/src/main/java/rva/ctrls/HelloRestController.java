package rva.ctrls;

import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = {"Root putanje"})
@RestController
public class HelloRestController {
	
	@ApiOperation(value = "Root")
	@RequestMapping("/")
	public String helloWorld() {
		return "Hello World!";
	}
}
