package rva.ctrls;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import rva.jpa.Student;
import rva.reps.StudentRepository;

@Api(tags = {"Student CRUD operacije"})
@RestController
public class StudentRestController {
	@Autowired
	private StudentRepository studentRepository;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@ApiOperation(value = "Vraća kolekciju svih studenata iz baze podataka")
	@GetMapping("student")
	public Collection<Student> getStudent() {
		return studentRepository.findAll();
	}
	
	@ApiOperation(value = "Vraća studenta iz baze podataka čiji je id vrednost prosleđena kao path varijabla")
	@GetMapping("student/{id}")
	public Student getStudent(@PathVariable("id") Integer id) {
		return studentRepository.getOne(id);
	}
	
	@ApiOperation(value = "Vraća kolekciju svih studenata iz baze podataka koji u nazivu sadrže string prosleđen kao path varijabla")
	@GetMapping("studentNaziv/{naziv}")
	public Collection<Student> getStudentByNaziv(@PathVariable("naziv") String naziv) {
		String[] splited = naziv.split("\\s+");
		if(splited.length==1)
		return studentRepository.findByImeContainingIgnoreCaseOrPrezimeContainingIgnoreCase(naziv,naziv);
		else
		return studentRepository.findByImeContainingIgnoreCaseAndPrezimeContainingIgnoreCase(splited[0],splited[1]);	
	}

	@ApiOperation(value = "Vraća kolekciju svih studenata iz baze podataka koji u indeksu sadrže string prosleđen kao path varijabla")
	@GetMapping("studentIndeks/{indeks}")
	public Collection<Student> getStudentByIndeks(@PathVariable("indeks") String indeks) {
		return studentRepository.findByBrojIndeksaContainingIgnoreCase(indeks);	
	}
	
	@ApiOperation(value = "Briše studenta iz baze podataka čiji je id vrednost prosleđena kao path varijabla")
	@CrossOrigin
	@DeleteMapping("student/{id}")
	public ResponseEntity<Student> deleteStudent(@PathVariable("id") Integer id) {
		if(!studentRepository.existsById(id))
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		studentRepository.deleteById(id);
		if(id==11)
			jdbcTemplate.execute("INSERT INTO \"student\" (\"id\",\"ime\",\"prezime\",\"broj_indeksa\",\"status\",\"departman\") VALUES (11,'TEST Indeks','TEST Ime','TEST Prezime',1,1)");
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	// insert
	@ApiOperation(value = "Upisuje studenta u bazu podataka")
	@CrossOrigin
	@PostMapping("student")
	public ResponseEntity<Student> insertStudent(@RequestBody Student student) {
		if(!studentRepository.existsById(student.getId())) {
			studentRepository.save(student);
			return new ResponseEntity<	>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.CONFLICT);
	}
	
	// update
	@ApiOperation(value = "Modifikuje postojećeg studenta u bazi podataka")
	@CrossOrigin
	@PutMapping("student")
	public ResponseEntity<Student> updateStudent(@RequestBody Student student) {
		if (!studentRepository.existsById(student.getId())) {
			
			System.out.print("wtf "+student.getId()+ " "+!studentRepository.existsById(student.getId()));
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		}
		studentRepository.save(student);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
