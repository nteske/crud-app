package rva.reps;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import rva.jpa.Student;

public interface StudentRepository extends JpaRepository<Student, Integer> {
	Collection<Student> findByImeContainingIgnoreCaseAndPrezimeContainingIgnoreCase(String ime,String prezime);
	Collection<Student> findByImeContainingIgnoreCaseOrPrezimeContainingIgnoreCase(String ime,String prezime);
	Collection<Student> findByBrojIndeksaContainingIgnoreCase(String indeks);

}
