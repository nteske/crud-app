DROP TABLE IF EXISTS fakultet CASCADE;
DROP TABLE IF EXISTS status CASCADE;
DROP TABLE IF EXISTS departman CASCADE;
DROP TABLE IF EXISTS student CASCADE;
 
DROP SEQUENCE IF EXISTS fakultet_seq;
DROP SEQUENCE IF EXISTS status_seq;
DROP SEQUENCE IF EXISTS departman_seq;
DROP SEQUENCE IF EXISTS student_seq;

CREATE TABLE fakultet (
    id INTEGER NOT NULL,
    naziv VARCHAR(100) NOT NULL,
    sediste VARCHAR(50) NOT NULL
);
 
CREATE TABLE departman(
    id INTEGER NOT NULL,
    naziv VARCHAR(100) NOT NULL,
    oznaka VARCHAR(10) NOT NULL,
    fakultet INTEGER NOT NULL
);
 
CREATE TABLE student(
    id INTEGER NOT NULL,
    ime VARCHAR(50) NOT NULL,
    prezime VARCHAR(50) NOT NULL,
    broj_indeksa VARCHAR(20) NOT NULL,
    status INTEGER NOT NULL,
    departman INTEGER NOT NULL
);
 
CREATE TABLE status (
    id INTEGER NOT NULL,
    naziv VARCHAR(100) NOT NULL,
    oznaka VARCHAR(10) NOT NULL
);

ALTER TABLE fakultet ADD CONSTRAINT PK_Fakultet
	PRIMARY KEY(id);

ALTER TABLE departman ADD CONSTRAINT PK_Departman
	PRIMARY KEY(id);

ALTER TABLE student ADD CONSTRAINT PK_Student
	PRIMARY KEY(id);

ALTER TABLE status ADD CONSTRAINT PK_Status
	PRIMARY KEY(id);
	
ALTER TABLE departman ADD CONSTRAINT FK_Departman_Fakultet
    FOREIGN KEY(fakultet) REFERENCES fakultet(id);
	
ALTER TABLE student ADD CONSTRAINT FK_Student_Status
    FOREIGN KEY(status) REFERENCES status(id);
	
ALTER TABLE student ADD CONSTRAINT FK_Student_Departman
    FOREIGN KEY(departman) REFERENCES departman(id);
	
CREATE INDEX IDXFK_Departman_Fakultet
    ON departman(fakultet);
	
CREATE INDEX IDXFK_Student_Status
    ON student(status);
	
CREATE INDEX IDXFK_Student_Departman
    ON student(departman);
	
CREATE SEQUENCE fakultet_seq
INCREMENT 1;

CREATE SEQUENCE departman_seq
INCREMENT 1;

CREATE SEQUENCE student_seq
INCREMENT 1;

CREATE SEQUENCE status_seq
INCREMENT 1;