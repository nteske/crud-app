-- dodavanje u fakultet
INSERT INTO "fakultet" ("id", "naziv", "sediste")
VALUES (nextval('fakultet_seq'), 'Fakultet tehničkih nauka', 'Novi Sad');
INSERT INTO "fakultet" ("id", "naziv", "sediste")
VALUES (nextval('fakultet_seq'), 'Pravni fakultet', 'Novi Sad');
INSERT INTO "fakultet" ("id", "naziv", "sediste")
VALUES (nextval('fakultet_seq'), 'Prirodno matematički fakultet', 'Novi Sad');

-- dodavanje u status
INSERT INTO "status" ("id", "naziv", "oznaka")
VALUES (nextval('status_seq'), 'Budžetski', 'BU');
INSERT INTO "status" ("id", "naziv", "oznaka")
VALUES (nextval('status_seq'), 'Samofinansirajući', 'SF');

-- dodavanje u departman
INSERT INTO "departman" ("id", "naziv", "oznaka", "fakultet")
VALUES (nextval('departman_seq'), 'Departman za industrijsko inženjerstvo i menadžment', 'DIIM', 1);
INSERT INTO "departman" ("id", "naziv", "oznaka", "fakultet")
VALUES (nextval('departman_seq'), 'Departman za elektrotehniku', 'DE', 1);
INSERT INTO "departman" ("id", "naziv", "oznaka", "fakultet")
VALUES (nextval('departman_seq'), 'Departman za matematiku i informatiku', 'DMI', 3);
INSERT INTO "departman" ("id", "naziv", "oznaka", "fakultet")
VALUES (nextval('departman_seq'), 'Departman za biologiju i ekologiju', 'DBE', 3);
INSERT INTO "departman" ("id", "naziv", "oznaka", "fakultet")
VALUES (nextval('departman_seq'), 'Departman za geografiju, turizam i hotelijerstvo', 'DGTH', 3);

-- dodavanje u studenta
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Nikola', 'Nikolić', 'IT1/2016', 1, 1);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Jovan', 'Kron', 'IT28/2016', 1, 1);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Maja', 'Karanović', 'IT21/2016', 1, 1);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Ana', 'Nikolić', 'GT1/2016', 2, 5);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Ivan', 'Savić', 'MI666/2016', 2, 3);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Sara', 'Ivković', '341/2014', 1, 4);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Petar', 'Vučković', 'EE12/2017', 1, 2);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Marko', 'Katić', '777/2017', 1, 4);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Bojana', 'Marinković', '231/2016', 2, 4);
INSERT INTO "student" ("id", "ime", "prezime", "broj_indeksa", "status", "departman")
VALUES (nextval('student_seq'), 'Tara', 'Tucić', '91/2018', 1, 3);

select * from departman;
select *from student;
