import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StatusService } from 'src/app/services/status.service';
import { Status } from 'src/app/models/status';
@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.css']
})
export class StatusDialogComponent implements OnInit {

  public flag: number;

  constructor(public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<StatusDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Status,
    public statusService: StatusService) { }

  ngOnInit() {
  }


  public add(): void {
    this.data.id = -1;
    this.statusService.addStatus(this.data);
    this.snackBar.open("Uspešno dodat status: " + this.data.naziv, "U redu", {
      duration: 2500,
    });
  }

  public update(): void {
    this.statusService.updateStatus(this.data);
    this.snackBar.open("Uspešno modifikovan status: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public delete(): void {
    this.statusService.deleteStatus(this.data.id);
    this.snackBar.open("Uspešno obrisan status: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public cancel(): void {
    this.dialogRef.close();
    this.snackBar.open("Odustali ste", "U redu", {
      duration: 1000,
    });
  }
}
