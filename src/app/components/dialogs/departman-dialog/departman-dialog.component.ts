import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { DepartmanService } from 'src/app/services/departman.service';
import { Departman } from 'src/app/models/departman';
import { Fakultet } from 'src/app/models/fakultet';
import { FakultetService } from 'src/app/services/fakultet.service';

@Component({
  selector: 'app-departman-dialog',
  templateUrl: './departman-dialog.component.html',
  styleUrls: ['./departman-dialog.component.css']
})
export class DepartmanDialogComponent implements OnInit {

  public flag: number;

  constructor(public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<DepartmanDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Departman,
    public departmanService: DepartmanService) { }

  ngOnInit() {

  }

  compareTo(a, b) {
    return a.id = b.id;
  }



  public add(): void {
    this.data.id = -1;
    this.departmanService.addDepartman(this.data);
    this.snackBar.open("Uspešno dodat departman: " + this.data.naziv, "U redu", {
      duration: 2500,
    });
  }

  public update(): void {
    this.departmanService.updateDepartman(this.data);
    this.snackBar.open("Uspešno modifikovan departman: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public delete(): void {
    this.departmanService.deleteDepartman(this.data.id);
    this.snackBar.open("Uspešno obrisan departman: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public cancel(): void {
    this.dialogRef.close();
    this.snackBar.open("Odustali ste", "U redu", {
      duration: 1000,
    });
  }

}
