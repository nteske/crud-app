import { Component, OnInit, Inject } from '@angular/core';
import { MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { StudentService } from 'src/app/services/student.service';
import { Student } from 'src/app/models/student';
import { Departman } from 'src/app/models/departman';
import { DepartmanService } from 'src/app/services/departman.service';
import { StatusService } from 'src/app/services/status.service';
import { Status } from 'src/app/models/status';

@Component({
  selector: 'app-student-dialog',
  templateUrl: './student-dialog.component.html',
  styleUrls: ['./student-dialog.component.css']
})
export class StudentDialogComponent implements OnInit {

  public flag: number;
  departmani: Departman[];
  statusi: Status[];
  constructor(public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<StudentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: Student,
    public studentService: StudentService,
    public departmanService: DepartmanService,
    public statusService: StatusService) { }

  ngOnInit() {
    this.departmanService.getAllDepartman().subscribe(departmani =>
      this.departmani = departmani);
    this.statusService.getAllStatus().subscribe(statusi =>
        this.statusi = statusi);
  }

  compareTo(a, b) {
    return a.id = b.id;
  }

  onChange(status:Status,departman:Departman) {
    this.data.status = status;
    this.data.departman = departman;
  }

  public add(): void {
    this.data.id = -1;
    this.studentService.addStudent(this.data);
    this.snackBar.open("Uspešno dodat student: " + this.data.ime+" "+this.data.prezime, "U redu", {
      duration: 2500,
    });
  }

  public update(): void {
    this.studentService.updateStudent(this.data);
    this.snackBar.open("Uspešno modifikovan student: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public delete(): void {
    this.studentService.deleteStudent(this.data.id);
    this.snackBar.open("Uspešno obrisan student: " + this.data.id, "U redu", {
      duration: 2000,
    });
  }

  public cancel(): void {
    this.dialogRef.close();
    this.snackBar.open("Odustali ste", "U redu", {
      duration: 1000,
    });
  }
}
