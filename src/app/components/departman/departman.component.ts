import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Departman } from 'src/app/models/departman';
import { Fakultet } from 'src/app/models/fakultet';
import { MatDialog, MatTableDataSource, MatSort, MatPaginator } from '@angular/material';
import { DepartmanService } from 'src/app/services/departman.service';
import { DepartmanDialogComponent } from '../dialogs/departman-dialog/departman-dialog.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-departman',
  templateUrl: './departman.component.html',
  styleUrls: ['./departman.component.css']
})
export class DepartmanComponent implements OnInit {

  displayedColumns = ['id', 'naziv', 'oznaka','fakultet', 'actions'];
  dataSource: MatTableDataSource<Departman>;

  @Input() selektovanFakultet: Fakultet;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(public departmanService: DepartmanService,private route: ActivatedRoute,
              public dialog: MatDialog) { }

  ngOnInit() {

  }

  ngOnChanges() {
    if (this.selektovanFakultet.id) {
      this.route.paramMap.subscribe(() => {
      this.loadData();//
      });
    }
  }

  public loadData() {
    this.departmanService.getDepartmanZaFakultet(this.selektovanFakultet.id)
  .subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      //pretraga po nazivu ugnježdenog objekta
      this.dataSource.filterPredicate = (data, filter: string) => {
        const accumulator = (currentTerm, key) => {
          return key === 'fakultet' ? currentTerm + data.fakultet.naziv : currentTerm + data[key];
        };
        const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
        const transformedFilter = filter.trim().toLowerCase();
        return dataStr.indexOf(transformedFilter) !== -1;
      };

        //sortiranje po nazivu ugnježdenog objekta
        this.dataSource.sortingDataAccessor = (data, property) => {
        switch(property) {
          case 'id': return data[property];

          default: return data.naziv.toLocaleLowerCase();
        }
      };

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

  public openDialog(flag: number, id: number, naziv: string, oznaka: string,fakultet:Fakultet) {
    const dialogRef = this.dialog.open(DepartmanDialogComponent, {
      data: {
        i: id, id: id, naziv: naziv, oznaka: oznaka,fakultet:fakultet
      }
    });
    dialogRef.componentInstance.flag = flag;
    if (flag == 1)
      dialogRef.componentInstance.data.fakultet = this.selektovanFakultet;

    dialogRef.afterClosed().subscribe(result => {
      if (result == 1)
        this.loadData();
    });
  }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
  }


}
