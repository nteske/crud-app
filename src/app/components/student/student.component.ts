import { Component, OnInit, ViewChild } from '@angular/core';
import { Student } from 'src/app/models/student';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { Observable } from 'rxjs';
import { StudentService } from 'src/app/services/student.service';
import { StudentDialogComponent } from '../dialogs/student-dialog/student-dialog.component';
import { Departman } from 'src/app/models/departman';
import { Status } from 'src/app/models/status';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {


  displayedColumns = ['id', 'ime', 'prezime','brojIndeksa','status.naziv','departman.naziv', 'actions'];
  dataSource: MatTableDataSource<Student>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public httpClient: HttpClient,private route: ActivatedRoute,
    public dialog:MatDialog,
    public studentService: StudentService) { }

  public loadData() {
    this.route.paramMap.subscribe(() => {
     this.studentService.getAllStudent().subscribe(data=>{
      this.dataSource =new MatTableDataSource(data);

      this.dataSource.filterPredicate = (data, filter: string) => {
        const accumulator = (currentTerm, key) => {
          return key === 'departman' ? currentTerm + data.departman.naziv : currentTerm + data[key];
        };
        const dataStr = Object.keys(data).reduce(accumulator, '').toLowerCase();
        const transformedFilter = filter.trim().toLowerCase();
        return dataStr.indexOf(transformedFilter) !== -1;
      };

    this.dataSource.sortingDataAccessor = (data, property) => {
      switch (property) {
        case 'id': return data[property];
        case 'departman.naziv': return data.departman.naziv.toLocaleLowerCase();
        case 'status.naziv': return data.status.naziv.toLocaleLowerCase();
        default: return data[property].toLocaleLowerCase();
      }
    };

    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  });
});
  }

  ngOnInit() {
  this.loadData();
  }

  public openDialog(flag: number, id: number, ime: string, prezime: string,
    brojIndeksa:string,status:Status,departman:Departman) {
    const dialogRef = this.dialog.open(StudentDialogComponent,
                      { data: { id: id, ime: ime, prezime: prezime,brojIndeksa:brojIndeksa,
                      status:status,departman:departman } });
    dialogRef.componentInstance.flag = flag;
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1)
        this.loadData();
    });
  }
  applyFilter(filterValue: string){
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
  }
}
