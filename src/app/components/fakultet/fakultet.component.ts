import { Component, OnInit, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { Fakultet } from '../../models/fakultet';
import { FakultetService } from '../../services/fakultet.service';
import { HttpClient } from '@angular/common/http';
import { FakultetDialogComponent } from '../dialogs/fakultet-dialog/fakultet-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { MatDialog, MatTableDataSource, MatPaginator, MatSort } from '@angular/material';

@Component({
  selector: 'app-fakultet',
  templateUrl: './fakultet.component.html',
  styleUrls: ['./fakultet.component.css']
})
export class FakultetComponent implements OnInit {

  displayedColumns = ['id', 'naziv', 'sediste', 'actions'];
  dataSource: MatTableDataSource<Fakultet>;

  selektovanFakultet: Fakultet;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(public httpClient: HttpClient,private route: ActivatedRoute,
    public dialog:MatDialog,
    public fakultetService: FakultetService) { }

  public loadData() {
    this.fakultetService.getAllFakultet().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);


       //sortiranje po nazivu ugnježdenog objekta
       this.dataSource.sortingDataAccessor = (data, property) => {
        switch(property) {
          case 'id': return data[property];
          default: return data.naziv.toLocaleLowerCase();
        }
      };

      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }


  ngOnInit() {
  this.loadData();
  }

  public openDialog(flag: number, id: number, naziv: string, sediste: string) {
    const dialogRef = this.dialog.open(FakultetDialogComponent,
                      { data: { id: id, naziv: naziv, sediste: sediste } });
    dialogRef.componentInstance.flag = flag;
    dialogRef.afterClosed().subscribe(result => {
      if (result == 1)
        this.loadData();
    });
  }

  selectRow(row){
    this.selektovanFakultet = row;
  }

  applyFilter(filterValue: string){
    filterValue = filterValue.trim();
    filterValue = filterValue.toLocaleLowerCase();
    this.dataSource.filter = filterValue;
  }

}
