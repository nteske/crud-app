import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MatButtonModule,
  MatIconModule,
  MatListModule,
  MatSidenavModule,
  MatGridListModule,
  MatExpansionModule,
  MatTableModule,
  MatToolbarModule,
  MatSelectModule,
  MatSortModule,
  MatOptionModule,
  MatSnackBarModule,
  MatPaginatorModule,
  MatDialogModule,
  MatInputModule}  from '@angular/material';
import { HomeComponent } from './components/core/home/home.component';
import { AboutComponent } from './components/core/about/about.component';
import { AuthorComponent } from './components/core/author/author.component';
import { FakultetComponent } from './components/fakultet/fakultet.component';
import { StatusComponent } from './components/status/status.component';
import { StudentComponent } from './components/student/student.component';
import { DepartmanComponent } from './components/departman/departman.component';
import { FakultetService } from './services/fakultet.service';
import { FakultetDialogComponent } from './components/dialogs/fakultet-dialog/fakultet-dialog.component';
import { DepartmanService } from './services/departman.service';
import { StatusService } from './services/status.service';
import { StudentService } from './services/student.service';


import { FormsModule } from '@angular/forms';
import { StatusDialogComponent } from './components/dialogs/status-dialog/status-dialog.component';
import { StudentDialogComponent } from './components/dialogs/student-dialog/student-dialog.component';
import { DepartmanDialogComponent } from './components/dialogs/departman-dialog/departman-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    AuthorComponent,
    FakultetComponent,
    StatusComponent,
    StudentComponent,
    DepartmanComponent,
    FakultetDialogComponent,
    StatusDialogComponent,
    StudentDialogComponent,
    DepartmanDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    MatGridListModule,
    MatExpansionModule,
    MatTableModule,
    MatToolbarModule,
    MatOptionModule,
    MatSelectModule,
    HttpClientModule,
    MatSnackBarModule,
    MatDialogModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    FormsModule
  ],
  providers: [FakultetService,DepartmanService,StatusService,StudentService],
  entryComponents: [
    FakultetDialogComponent,StatusDialogComponent,StudentDialogComponent,DepartmanDialogComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
