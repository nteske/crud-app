import { Departman } from './departman';
import { Status } from './status';

export class Student {
    id: number;
    ime: string;
    prezime: string;
    broj_indeksa: string;
    status: Status;
    departman: Departman;
  }
