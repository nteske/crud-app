import { Injectable } from '@angular/core';
import { Departman } from '../models/departman';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DepartmanService {
  private readonly API_URL = 'http://localhost:8083/departman/';
  private readonly API_URL_BYID = 'http://localhost:8083/departmaniZaFakultetById/';

  dataChange: BehaviorSubject<Departman[]> = new BehaviorSubject<Departman[]>([]);
  constructor(private httpClient: HttpClient) { }

  public getAllDepartman(): Observable<Departman[]> {
    this.httpClient.get<Departman[]>(this.API_URL).subscribe(data => {
        this.dataChange.next(data);
    },
    (error: HttpErrorResponse) => {
        console.log(error.name + ' ' + error.message);
    });

    return this.dataChange.asObservable();
}
    public addDepartman(departman: Departman): void {
        this.httpClient.post(this.API_URL, departman).subscribe();
    }

    public updateDepartman(departman: Departman): void {
        this.httpClient.put(this.API_URL, departman).subscribe();
    }

    public deleteDepartman(id: number): void {
        console.log(this.API_URL + id);
        this.httpClient.delete(this.API_URL + id).subscribe();
    }

    public getDepartmanZaFakultet(idFakulteta): Observable<Departman[]> {
      this.httpClient.get<Departman[]>(this.API_URL_BYID + idFakulteta)
      .subscribe(data => {
          this.dataChange.next(data);
      },
      (error: HttpErrorResponse) => {
          console.log(error.name + ' ' + error.message);
      });
      return this.dataChange.asObservable();
  }

}
